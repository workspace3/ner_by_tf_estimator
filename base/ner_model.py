# -*- coding: utf-8 -*-
"""GloVe Embeddings + bi-LSTM + CRF
created by "Guillaume Genthial"
edited by wen @2019/11/6
"""

import functools
import json
import logging
from pathlib import Path
import sys

import numpy as np
import tensorflow as tf
from tf_metrics import precision, recall, f1

DATADIR = '../data/'

# Logging
Path('../tmp').mkdir(exist_ok=True)
tf.logging.set_verbosity(logging.INFO)
handlers = [
    logging.FileHandler('../tmp/main.log'),
    logging.StreamHandler(sys.stdout)
]
logging.getLogger('tensorflow').handlers = handlers


def model_fn(features, labels, mode, params):
    # For serving, features are a bit different
    if isinstance(features, dict):
        features = features['words'], features['nwords']

    # Read vocabs and inputs
    dropout = params['dropout']
    words, nwords = features
    training = (mode == tf.estimator.ModeKeys.TRAIN)
    vocab_words = tf.contrib.lookup.index_table_from_file(
        params['words'], num_oov_buckets=params['num_oov_buckets'])
    with Path(params['tags']).open() as f:
        indices = [idx for idx, tag in enumerate(f) if tag.strip() != 'O']
        num_tags = len(indices) + 1

    # Word Embeddings
    word_ids = vocab_words.lookup(words)
    variable = tf.Variable(tf.ones(shape=(params["vocab_size"], params["dim"])),
                           dtype=tf.float32, trainable=training)
    embeddings = tf.nn.embedding_lookup(variable, word_ids)
    embeddings = tf.layers.dropout(embeddings, rate=dropout, training=training)

    # LSTM
    t = tf.transpose(embeddings, perm=[1, 0, 2])
    lstm_cell_fw = tf.nn.rnn_cell.GRUCell(params['lstm_size'])
    lstm_cell_bw = tf.nn.rnn_cell.GRUCell(params['lstm_size'])
    (output_fw, output_bw), _ = tf.nn.bidirectional_dynamic_rnn(
        lstm_cell_fw, lstm_cell_bw, t, dtype=tf.float32, sequence_length=nwords, time_major=True)
    output = tf.concat([output_fw, output_bw], axis=-1)
    output = tf.transpose(output, perm=[1, 0, 2])
    output = tf.layers.dropout(output, rate=dropout, training=training)

    # CRF
    logits = tf.layers.dense(output, num_tags)
    crf_params = tf.get_variable("crf", [num_tags, num_tags], dtype=tf.float32)
    pred_ids, _ = tf.contrib.crf.crf_decode(logits, crf_params, nwords)

    if mode == tf.estimator.ModeKeys.PREDICT:
        # Predictions
        reverse_vocab_tags = tf.contrib.lookup.index_to_string_table_from_file(
            params['tags'])
        pred_strings = reverse_vocab_tags.lookup(tf.to_int64(pred_ids))
        predictions = {
            'pred_ids': pred_ids,
            'tags': pred_strings
        }
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)
    else:
        # Loss
        vocab_tags = tf.contrib.lookup.index_table_from_file(params['tags'])
        tags = vocab_tags.lookup(labels)
        log_likelihood, _ = tf.contrib.crf.crf_log_likelihood(
            logits, tags, nwords, crf_params)
        loss = tf.reduce_mean(-log_likelihood)

        # Metrics
        weights = tf.sequence_mask(nwords)
        metrics = {
            'acc': tf.metrics.accuracy(tags, pred_ids, weights),
            'precision': precision(tags, pred_ids, num_tags, indices, weights),
            'recall': recall(tags, pred_ids, num_tags, indices, weights),
            'f1': f1(tags, pred_ids, num_tags, indices, weights),
        }
        for metric_name, op in metrics.items():
            tf.summary.scalar(metric_name, op[1])

        if mode == tf.estimator.ModeKeys.EVAL:
            return tf.estimator.EstimatorSpec(
                mode, loss=loss, eval_metric_ops=metrics)

        elif mode == tf.estimator.ModeKeys.TRAIN:
            train_op = tf.train.AdamOptimizer().minimize(
                loss, global_step=tf.train.get_or_create_global_step())
            return tf.estimator.EstimatorSpec(
                mode, loss=loss, train_op=train_op)


class BlstmCrfNer:
    def __init__(self, params, model_dir):
        self.params = params
        # Estimator
        cfg = tf.estimator.RunConfig(save_checkpoints_secs=120)
        self.estimator = tf.estimator.Estimator(model_fn, model_dir, cfg, self.params)
        Path(self.estimator.eval_dir()).mkdir(parents=True, exist_ok=True)
        self.model_dir = model_dir
        self.first_run = True
        self.predictions = None
        self.sent = None
        self.is_closed = False

    def generator_fn_from_input(self):
        while not self.is_closed:
            words = list(self.sent)
            yield (words, len(words)), []

    @staticmethod
    def generator_fn_from_file(data):
        with Path(data).open('r', encoding="utf8") as fi:
            curr_x, curr_y = [], []
            for line in fi.readlines():
                if "\n" == line:
                    yield (curr_x, len(curr_x)), curr_y
                    curr_x, curr_y = [], []
                else:
                    parts = line.strip().split(" ")
                    if len(parts) != 2:
                        print("数据错误：{}".format(line), file=sys.stderr)
                    else:
                        curr_x.append(parts[0])
                        curr_y.append(parts[1])

    def input_fn(self, data, is_file=True, params=None, shuffle_and_repeat=False):
        params = params if params is not None else {}
        shapes = (([None], ()), [None])
        types = ((tf.string, tf.int32), tf.string)
        defaults = (('<pad>', 0), 'O')
        if not is_file:
            dataset = tf.data.Dataset.from_generator(self.generator_fn_from_input,
                                                     output_shapes=shapes, output_types=types)
        else:
            dataset = tf.data.Dataset.from_generator(
                functools.partial(self.generator_fn_from_file, data),
                output_shapes=shapes, output_types=types)

        if shuffle_and_repeat:
            dataset = dataset.shuffle(params['buffer']).repeat(params['epochs'])
            dataset = (dataset
                       .padded_batch(params.get('batch_size', 20), shapes, defaults)
                       .prefetch(1))
        else:
            dataset = (dataset
                       .padded_batch(1, shapes, defaults))
        return dataset

    def train(self, train_file, dev_file):
        train_inpf = functools.partial(self.input_fn, train_file,
                                       self.params, shuffle_and_repeat=True)
        eval_inpf = functools.partial(self.input_fn, dev_file)
        train_spec = tf.estimator.TrainSpec(input_fn=train_inpf)
        eval_spec = tf.estimator.EvalSpec(input_fn=eval_inpf, throttle_secs=120)
        tf.estimator.train_and_evaluate(self.estimator, train_spec, eval_spec)

    def predict(self, sentence):
        self.sent = sentence
        if self.first_run:
            is_file = False
            self.predictions = self.estimator.predict(functools.partial(self.input_fn, "", is_file=is_file))
            self.first_run = False
        tag_pred = next(self.predictions)['tags']
        return tag_pred

    def close(self):
        self.is_closed = True


if __name__ == "__main__":
    import timeit
    params = {
        "vocab_size": 7841,
        'dim': 300,
        'dropout': 0.5,
        'num_oov_buckets': 1,
        'epochs': 10,
        'batch_size': 32,
        'buffer': 15000,
        'lstm_size': 100,
        'words': str(Path(DATADIR, 'vocab.words.txt')),
        'tags': str(Path(DATADIR, 'vocab.tags.txt'))
    }
    model_dir = "../model"
    my_model = BlstmCrfNer(params, model_dir)
    train_file = "../data/train_ner.txt"
    dev_file = "../data/dev_ner.txt"
    # my_model.train(train_file, dev_file)

    while True:
        sentence = input("请输入：")
        if "q" == sentence:
            break
        s = timeit.default_timer()
        result = my_model.predict(sentence)
        e = timeit.default_timer()
        print("NER结果: {0:}\n耗时：{1:.5f}秒".format(str(result), e-s))
    my_model.close()
